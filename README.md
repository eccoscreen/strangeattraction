![screen](media/screen.png)
[vimeo link](https://vimeo.com/228712070)

# strange attraction

TouchDesigner audio reactive GPU particle system with attraction and repel.

# controls

Use the mouse to control the particles.

Pressing '1' on the keyboard will reset the particle positions.
